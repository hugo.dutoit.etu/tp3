import Page from './Page.js';

export default class AddPizzaPage extends Page {
	render() {
		return /*html*/ `
			<form class="pizzaForm">
				<label>
					Nom :
					<input type="text" name="name">
				</label>
				<button type="submit">Ajouter</button>
			</form>`;
	}

	mount(element) {
		super.mount(element);
		this.element.addEventListener('submit', this.submit);
	}

	submit(e) {
		let inputVal = e.currentTarget.querySelector('input[name=name]').value;
		if (inputVal == '') {
			alert('Chaine VIDE !!!');
		} else {
			alert(`La pizza ${inputVal} a ete Ajoutee`);
		}
		e.preventDefault();
	}
}
