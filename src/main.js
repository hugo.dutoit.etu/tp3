import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import Component from './components/Component.js';
import PizzaForm from './pages/PizzaForm.js';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');
Router.menuElement = document.querySelector('.mainMenu');
const pizzaList = new PizzaList([]),
	aboutPage = new Component('p', null, 'ce site est génial'),
	pizzaForm = new PizzaForm();
Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];
Router.navigate(new URL(document.location).pathname); // affiche une page vide

document.querySelector('.logo').innerHTML +=
	"<small>les pizzas c'est la vie</small>";
console.log(document.querySelectorAll('footer a')[1].getAttribute('href'));
const firstlink = document.querySelector('header li a');
firstlink.setAttribute('class', firstlink.getAttribute('class') + ' active');

const news = document.querySelector('.newsContainer');
news.setAttribute('style', '');
document.querySelector('.closeButton').addEventListener('click', e => {
	news.setAttribute('style', 'display: none');
	e.preventDefault();
});
window.onpopstate = ev => {
	console.log(new URL(document.location).pathname);
};
