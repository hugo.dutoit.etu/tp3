export default class Router {
	static titleElement;
	static contentElement;
	static routes = [];

	static #menuElement;
	static set menuElement(element) {
		this.#menuElement = element;
		console.log('plop');
		// au clic sur n'importe quel lien contenu dans "element"
		// déclenchez un appel à Router.navigate(path)
		// où "path" est la valeur de l'attribut `href=".."` du lien cliqué
		for (let y = 0; y < this.#menuElement.children.length; y++) {
			this.#menuElement.querySelectorAll('a').forEach(link => {
				link.addEventListener('click', e => {
					this.#menuElement.querySelectorAll('a').forEach(links => {
						links.classList.remove('active');
					});
					link.classList.add('active');
					Router.navigate(link.getAttribute('href'));
					e.preventDefault();
				});
			});
		}
	}
	static navigate(path) {
		const route = this.routes.find(route => route.path === path);
		if (route) {
			this.titleElement.innerHTML = `<h1>${route.title}</h1>`;
			this.contentElement.innerHTML = route.page.render();
			route.page.mount?.(this.contentElement);
			window.history.pushState(
				null,
				null,
				new URL(window.location).origin + path
			);
		}
	}
}
